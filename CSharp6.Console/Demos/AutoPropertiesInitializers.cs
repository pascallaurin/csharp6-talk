﻿namespace CSharp6.ConsoleApp.Demos
{
    public class AutoPropertiesInitializers
    {
        public string First { get; private set; } = "Pascal";
        public string Last { get; private set; } = "Laurin";
        // public string FullName { get; set; } = First + " " + Last;

        public void MyMethod()
        {
            First = "Lacsap";
        }
    }
}