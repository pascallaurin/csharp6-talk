﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CSharp6.ConsoleApp.Demos
{
    public class AwaitInCatchAndFinally
    {
        public async static Task MyMethod()
        {
            var client = new HttpClient();

            try
            {
                var tryResult = await client.GetAsync("http://www.try.com");
                Console.WriteLine($"Try: {tryResult.StatusCode}");
            }
            catch (Exception)
            {
                var catchResult = await client.GetAsync("http://www.catch.com");
                Console.WriteLine($"Catch: {catchResult.StatusCode}");
            }
            finally
            {
                var finallyResult = await client.GetAsync("http://www.finally.com");
                Console.WriteLine($"Finally: {finallyResult.StatusCode}");
            }
        }
    }
}