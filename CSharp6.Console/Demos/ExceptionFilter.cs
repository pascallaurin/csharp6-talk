﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace CSharp6.ConsoleApp.Demos
{
    public class ExceptionFilter
    {
        public static void MyMethod()
        {
            try
            {
                throw new Exception("Bar");
            }
            catch (Exception e) when (e.Message == "Foo")
            {
                Console.WriteLine("Was Foo");
            }
            catch (Exception e)
            {
                // if (e.Message == "Foo")
                // {
                //     Console.WriteLine("Was Foo");
                //     throw;
                // }

                Console.WriteLine(e);
            }
        }

        #region AggregateException

        public static void AggregateException()
        {
            // https://msdn.microsoft.com/en-us/library/system.aggregateexception.flatten(v=vs.110).aspx
            var task1 = Task.Factory.StartNew(() =>
            {
                Task.Factory.StartNew(() =>
                {
                    Task.Factory.StartNew(() =>
                    {
                        throw new InvalidOperationException("Attached child2 faulted.");
                    }, TaskCreationOptions.AttachedToParent);

                    throw new InvalidOperationException("Attached child1 faulted.");
                }, TaskCreationOptions.AttachedToParent);
            });

            try
            {
                task1.Wait();
            }
            catch (AggregateException ae) when (ae.Flatten().InnerExceptions.Any(e => e is InvalidOperationException))
            {
                Console.WriteLine("Yep, some InvalidOperationException found");
            }
        }

        #endregion

        public static void Logging()
        {
            try
            {
                throw new Exception("Bar");
            }
            catch (Exception e) when (Log(e.Message)) { }
            catch
            {
                Console.WriteLine("Do something about it!");
            }
        }

        private static bool Log(string message)
        {
            Console.WriteLine($"Message: {message}");
            return false;
        }
    }
}