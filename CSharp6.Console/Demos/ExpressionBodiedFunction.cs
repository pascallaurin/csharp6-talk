﻿using System;

namespace CSharp6.ConsoleApp.Demos
{
    public class ExpressionBodiedFunction
    {
        public ExpressionBodiedFunction()
        {
            First = "Lacsap";
            // Last = "Nirual";
        }

        public string First = "Pascal";
        public string Last => "Laurin";
        public string FullName => First + " " + Last;

        public void MyMethod()
        {
            First = "Lacsap";
            // Last = "Nirual";
        }

        #region Functions

        public int SomeFct(int a, int b) => a * b + a;

        public string GetFullName() => $"{First} {Last}";

        public void Print() => Console.WriteLine(GetFullName());

        #endregion

        #region Indexer

        public string this[long id] => (id * 2).ToString();

        #endregion
    }
}