﻿namespace CSharp6.ConsoleApp.Demos
{
    public class GetterOnlyAutoProperties
    {
        public GetterOnlyAutoProperties()
        {
            Last = "Nirual";
            FullName = First + " " + Last;
        }

        public string First { get; } = "Pascal";
        public string Last { get; } = "Laurin";
        public string FullName { get; }

        public void MyMethod()
        {
            // First = "Lacsap";
        }
    }
}