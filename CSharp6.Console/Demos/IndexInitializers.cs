﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace CSharp6.ConsoleApp.Demos
{
    public class IndexInitializers
    {
        public static void MyMethod()
        {
            var array = new Dictionary<int, string>
            {
                [2] = "Two",
                [12] = "Twelve",
                [5] = "Five"
            };
        }

        #region Json

        public static void Json()
        {
            // var j = new JObject();
            // j["firstName"] = "Pascal";
            // j["lastName"] = "Laurin";

            var j = new JObject
            {
                ["firstName"] = "Pascal",
                ["lastName"] = "Laurin"
            };

            Console.WriteLine(j);

            Console.WriteLine(new JObject {["firstName"] = "Pascal", ["lastName"] = "Laurin"});
        }

        #endregion
    }
}