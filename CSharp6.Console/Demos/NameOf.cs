﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CSharp6.ConsoleApp.Demos
{
    public static class NameOf
    {
        public static void MyMethod(string message)
        {
            // if (message == null) throw new ArgumentNullException("message");
            if (message == null) throw new ArgumentNullException(nameof(message));

            Console.WriteLine(nameof(CSharp6.ConsoleApp.Demos));
            Console.WriteLine(nameof(NameOf));
            Console.WriteLine(nameof(MyMethod));
            Console.WriteLine(nameof(message));

            const int foo = 42;
            Console.WriteLine(nameof(foo));
        }

        #region CallerMemberName

        private class MyViewModel : INotifyPropertyChanged
        {
            private string _name;

            public string Name
            {
                get { return _name; }
                set
                {
                    if (_name != value)
                    {
                        OnPropertyChanged();
                        _name = value;
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region SuppressMessage

        // [SuppressMessage("SomeCategory", "CC001", Target = nameof(message))]
        private static void SomeMethod(string message)
        {
        }

        #endregion
    }
}
