﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace CSharp6.ConsoleApp.Demos
{
    public class NullConditionalOperator
    {
        public static void MyMethod()
        {
            var order = Order.Create();

            // if (order != null && order.Customer != null && order.Customer.Name != null)
            // {
            //     Console.WriteLine(order.Customer.Name);
            // }

            if (order?.Customer?.Name != null)
            {
                Console.WriteLine(order.Customer.Name);
            }

            Console.WriteLine(order?.Items?.Count);
            Console.WriteLine(order?.Items?[0].Name);
        }

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            // var handler = PropertyChanged;
            // if (handler != null)
            // {
            //     handler(this, new PropertyChangedEventArgs(propertyName));
            // }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Nested classes

        private class Order
        {
            public Customer Customer { get; set; }
            public List<Item> Items { get; set; }

            public static Order Create()
            {
                return new Order();
            }
        }

        private class Customer
        {
            public string Name { get; set; }
        }

        private class Item
        {
            public string Name { get; set; }
        }

        #endregion
    }
}