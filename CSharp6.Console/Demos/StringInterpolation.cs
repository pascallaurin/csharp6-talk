﻿using System;
using System.Globalization;

namespace CSharp6.ConsoleApp.Demos
{
    public class StringInterpolation
    {
        public static void MyMethod()
        {
            const string firstName = "Pascal";
            const string lastName = "Laurin";

            // Console.WriteLine("My name is {0} {1} and I won {2:C2}", firstName, lastName, Amount);
            Console.WriteLine($"My name is {firstName} {lastName} and I won {Amount:C2}");

            var dog = new Dog();
            Console.WriteLine($"My dog {dog.Name,10} is {dog.Age} year{(dog.Age > 1 ? "s" : "")} old");
        }

        public static decimal Amount { get { return 5329.326m; } }

        private class Dog
        {
            public string Name { get { return "Max"; } }
            public int Age { get { return 2; } }
        }

        #region Formattable

        public static void FormattableStringMethod()
        {
            // IFormattable f = $"The amount is {Amount:c2}";
            FormattableString fs = $"The amount is {Amount:c2}";

            Console.WriteLine(fs.ToString(CultureInfo.GetCultureInfo("fr-FR")));
            Console.WriteLine(fs.ToString(CultureInfo.GetCultureInfo("en-UK")));
            Console.WriteLine(fs.ToString(CultureInfo.GetCultureInfo("ch-CH")));
        }

        #endregion
    }
}