﻿using System;
using System.Collections.Generic;

using static System.Console;
using static System.Math;
using static System.ConsoleColor;
using static CSharp6.ConsoleApp.Demos.OtherNamespace.MyExtensions;

namespace CSharp6.ConsoleApp.Demos
{
    public class UsingStatic
    {
        public void MyMethod()
        {
            // Console.WriteLine("Static");
            WriteLine(Cos(3.1415));

            // Console.ForegroundColor = ConsoleColor.DarkBlue;
            ForegroundColor = DarkBlue;

            new[] { 1, 2 }.Do(WriteLine);
            // new[] { 1, 2 }.DoMore(WriteLine);
        }
    }

    namespace OtherNamespace
    {
        public static class MyExtensions
        {
            public static void Do<T>(this IEnumerable<T> list, Action<T> action)
            {
                foreach (var item in list) action.Invoke(item);
            }
        }

        public static class MyExtensionsEx
        {
            public static void DoMore<T>(this IEnumerable<T> list, Action<T> action)
            {
                foreach (var item in list) action.Invoke(item);
            }
        }
    }
}