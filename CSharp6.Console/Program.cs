﻿using System;
using CSharp6.ConsoleApp.Demos;

namespace CSharp6.ConsoleApp
{
    static class Program
    {
        static void Main()
        {
            NameOfDemo();

            StringInterpolationDemo();

            NullConditionalOperator();

            IndexInitializers();

            ExceptionFilter();

            AwaitCatchFinally();

            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }

        private static void NameOfDemo()
        {
            Console.WriteLine("nameof:");
            NameOf.MyMethod("toto");
            Console.WriteLine();
        }

        private static void StringInterpolationDemo()
        {
            Console.WriteLine("string interpolation:");
            StringInterpolation.MyMethod();
            Console.WriteLine();
            StringInterpolation.FormattableStringMethod();
            Console.WriteLine();
        }

        private static void NullConditionalOperator()
        {
            Console.WriteLine("null conditional operator:");
            Demos.NullConditionalOperator.MyMethod();
            Console.WriteLine();
        }

        private static void IndexInitializers()
        {
            Console.WriteLine("index initializers:");
            Demos.IndexInitializers.Json();
            Console.WriteLine();
        }

        private static void ExceptionFilter()
        {
            Console.WriteLine("exception filter:");
            Demos.ExceptionFilter.MyMethod();
            Console.WriteLine();
            Demos.ExceptionFilter.AggregateException();
            Console.WriteLine();
            Demos.ExceptionFilter.Logging();
            Console.WriteLine();
        }

        private static void AwaitCatchFinally()
        {
            Console.WriteLine("await in catch and finally:");
            AwaitInCatchAndFinally.MyMethod().Wait();
            Console.WriteLine();
        }
    }
}
